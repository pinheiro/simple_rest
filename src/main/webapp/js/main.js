$(function() {

	// ## Funções utilitárias para utilização de templates ##
	_.templateSettings = {
		interpolate : /\{\{(.+?)\}\}/g
	};

	window.template = function(id) {
		return _.template($('#' + id).html());
	};

	window.templateByUrl = function(url) {
		return _.template(getTemplate(url));
	};

	window.getTemplate = function(url) {
		var data = "<h1> failed to load url : " + url + "</h1>";
		$.ajax({
			async : false,
			url : url,
			success : function(response) {
				data = response;
			}
		});
		return data;
	};

	// #####################################################
	// Definição dos Models
	// #####################################################
	var PessoaModel = Backbone.Model.extend({
		urlRoot : 'rs/pessoas',
		defaults : {
			id : null,
			cpf : '',
			nome : '',
			dataNascimento : '',
		}
	});

	// #####################################################
	// Definição dos Collections
	// #####################################################
	var PessoaCollection = Backbone.Collection.extend({
		url : 'rs/pessoas',
	});

	// #####################################################
	// Definição das Views
	// #####################################################
	var FormPessoa = Backbone.View.extend({
		template : templateByUrl('tpl/FormPessoa.html'),
		events : {
			'click #salvaPessoa' : 'salvaPessoa'
		},
		salvaPessoa : function() {
			var model = new PessoaModel({
				id : $('#inputId').val() || null,
				nome : $('#inputNome').val(),
				cpf : $('#inputCPF').val(),
				dataNascimento : $('#inputDataNascimento').val(),
			});

			model.save();
		},
		initialize : function() {
			var that = this;
			if (this.model) {
				this.model.fetch({
					success : function(_model, _resp, _options) {
						that.render();
					},
					error : function(_model, _resp, _options) {
						console.log("Erro ao obter model");
					}
				})
			} else {
				that.render();
			}
		},
		render : function() {
			var toShow = this.model || new PessoaModel();
			this.$el.html(this.template(toShow.toJSON()));
			return this;
		},
	});

	// ############################################################
	// Definição da tabela de pessoas
	// ############################################################

	var LinhaPessoas = Backbone.View.extend({
		tagName : 'tr',
		template : templateByUrl('tpl/TabelaPessoas.html'),

		initialize : function() {
			this.model.on('change', this.render, this);
			this.model.on('destroy', this.unrender, this);
		},

		events : {
			'click .remover' : 'removePessoa',
		},

		render : function() {
			if (this.model && this.model.get('id')) {
				var template = this.template(this.model.toJSON());
				this.$el.html(template);
			}
			return this;
		},

		removePessoa : function() {
			this.model.destroy();
		},

		unrender : function() {
			this.remove();
		},
	});

	var TabelaPessoas = Backbone.View.extend({
		tagName : 'table',
		className : 'table',

		initialize : function() {
			this.collection.on('add', this.addOne, this);
		},

		render : function() {
			this.addHeader();

			this.collection.each(this.addOne, this);
			return this;
		},

		addHeader : function() {
			this.$el.append('<thead><td >Id</td><td >Nome</td> <td >cpf</td><td class="actions">Ações</td></tr></thead>');
		},

		addOne : function(pessoa) {
			var linhaPessoa = new LinhaPessoas({
				model : pessoa
			});
			this.$el.append(linhaPessoa.render().el);
		}
	});

	// ############################################################
	// Definição do ROUTER
	// ############################################################
	var Router = Backbone.Router.extend({
		routes : {
			'pessoas' : 'gridPessoas',
			'novaPessoa' : 'pessoa',
			'editaPessoa/:id' : 'editPessoa',
		},
		gridPessoas : function() {
			var pessoas = new PessoaCollection();
			pessoas.fetch({
				success : function() {
					var tabelaPessoas = new TabelaPessoas({
						collection : pessoas,
					});
					$('.main-content').html(tabelaPessoas.render().el);
				},
				error : function() {

				}
			});
		},
		editPessoa : function(idPessoa) {
			var form = new FormPessoa({
				model : new PessoaModel({
					id : idPessoa,
				})
			});
			$('.main-content').html(form.render().el);
		},
		pessoa : function() {
			var form = new FormPessoa();
			$('.main-content').html(form.render().el);
		}
	});

	// Aki começa tudo
	APP_ROUTER = new Router();
	Backbone.history.start();
});